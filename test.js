
var assert = require("assert");

var X = require("./");

describe("rectsInRects", function () {


    describe("_keyDiff", function () {
        it("works on empty arrays", function () {
            assert.deepEqual(X._keyDiff([], []), {
                added: [],
                removed: [],
                visible: []
            });
        });

        it("works on exactly the same arrays", function () {
            assert.deepEqual(X._keyDiff(["a"], ["a"]), {
                added: [],
                removed: [],
                visible: ["a"]
            });
        });

        it("works when something is pushed", function () {
            assert.deepEqual(X._keyDiff(["a"], ["a", "b"]), {
                added: ["b"],
                removed: [],
                visible: ["a"]
            });
        });

        it("works when something is poped", function () {
            assert.deepEqual(X._keyDiff(["a", "b"], ["a"]), {
                added: [],
                removed: ["b"],
                visible: ["a"]
            });
        });

        it("works when something is un-shifted", function () {
            assert.deepEqual(X._keyDiff(["a"], ["b", "a"]), {
                added: ["b"],
                removed: [],
                visible: ["a"]
            });
        });

        it("works when something is shifted", function () {
            assert.deepEqual(X._keyDiff(["b", "a"], ["a"]), {
                added: [],
                removed: ["b"],
                visible: ["a"]
            });
        });
    });

    describe("swindler", function () {

        var rectUtils = {
            intersects: function (r1, r2) {
                /**
                 *
                 * http://stackoverflow.com/questions/2752349/fast-rectangle-to-rectangle-intersection
                 * return !(r2.left > r1.right ||
                 r2.right < r1.left ||
                 r2.top > r1.bottom ||
                 r2.bottom < r1.top);
                 */
                return !(r2[0] > r1[2] ||
                r2[2] < r1[0] ||
                r2[1] > r1[3] ||
                r2[3] < r1[1]);

            }
        };

        var singleItemLayout = function (rect) {

            return {
                rect: rect,
                findIntersections: function (rect2) {
                    if (!rectUtils.intersects(this.rect, rect2)) {
                        return null;
                    }
                    return ["theOne"];
                }
            }
        };

        var singleItemLayoutDelayed = function (nCalls, rect) {
            var nCallsLeft = nCalls;
            return {
                findIntersections: function (rect2) {
                    nCallsLeft--;
                    if (nCallsLeft >= 0) {
                        return null;
                    }

                    if (!rectUtils.intersects(rect, rect2)) {
                        return null;
                    }

                    return ["theOne"];
                }
            }
        };


        it("should select a rect if its fully the viewport", function (callback) {

            var onUpdates = function (viewport, updates) {
                assert.deepEqual(updates, {added: ["theOne"], removed: [], visible: []});
                return callback();
            };
            var viewPort = X({
                rect: [0, 0, 3, 3],
                rectUtils: rectUtils,
                layoutManager: singleItemLayout([1, 1, 2, 2]),
                updatesListener: onUpdates
            });
        });

        /**
         * a 3x3 grid of 9 squares of size 3 each, with the central square being at (0, 0, 3, 3)
         *
         * left center right
         * top middle bottom
         */
        var grid = {
            "top left": [-3, -3, 0, 0],
            "top center": [0, -3, 3, 0],
            "top right": [3, -3, 6, 0],
            "middle left": [-3, 0, 0, 3],
            "middle center": [0, 0, 3, 3],
            "middle right": [3, 0, 6, 3],
            "bottom left": [-3, 3, 0, 6],
            "bottom center": [0, 3, 6, 3],
            "bottom right": [3, 3, 6, 6]
        };

        Object.keys(grid).forEach(function (rectName) {
            it("should select the " + rectName + " rect adjoining the middle center rect", function (callback) {
                var onUpdates = function (viewport, updates) {
                    assert.deepEqual(updates, {added: ["theOne"], removed: [], visible: []});
                    return callback();
                };
                var viewPort = X({
                    rect: grid["middle center"],
                    rectUtils: rectUtils,
                    layoutManager: singleItemLayout(grid[rectName]),
                    updatesListener: onUpdates
                });
            });


            if (rectName != "middle center") {
                it("should not select the " + rectName + " rect if the view port is not touching", function (callback) {
                    var onUpdates = function (viewport, updates) {
                        assert.deepEqual(updates, {added: [], removed: [], visible: []});
                        return callback();
                    };
                    var viewPort = X({
                        rect: [0.1, 0.1, 2.9, 2.9],
                        rectUtils: rectUtils,
                        layoutManager: singleItemLayout(grid[rectName]),
                        updatesListener: onUpdates
                    });
                });
            }

            it("should select the " + rectName + " rect adjoining the middle center rect - async", function (callback) {
                var nCalls = 1;
                var onUpdates = function (viewport, updates) {
                    nCalls--;
                    if (nCalls >= 0) {
                        assert.deepEqual(updates, {added: [], removed: [], visible: []});
                        return;
                    }
                    assert.deepEqual(updates, {added: ["theOne"], removed: [], visible: []});
                    return callback();
                };

                var viewPort = X({
                    rect: grid["middle center"],
                    rectUtils: rectUtils,
                    layoutManager: singleItemLayoutDelayed(nCalls, grid[rectName]),
                    updatesListener: onUpdates
                });

                setTimeout(function () {
                    viewPort.move([0, 0, 3, 3]);
                }, 4);
            });


            it("should select the " + rectName + " rect adjoining the middle center rect - async 2 steps", function (callback) {
                var nCalls = 2;
                var onUpdates = function (viewport, updates) {
                    nCalls--;
                    if (nCalls >= 0) {
                        assert.deepEqual(updates, {added: [], removed: [], visible: []});
                        return;
                    }
                    assert.deepEqual(updates, {added: ["theOne"], removed: [], visible: []});
                    return callback();
                };

                var viewPort = X({
                    rect: [0.1, 0.1, 2.9, 2.9],
                    rectUtils: rectUtils,
                    layoutManager: singleItemLayoutDelayed(nCalls, grid[rectName]),
                    updatesListener: onUpdates
                });

                setTimeout(function () {
                    viewPort.move([0.1, 0.1, 2.9, 2.9]);
                    setTimeout(function () {
                        viewPort.move([0, 0, 3, 3]);
                    }, 4);
                }, 4);
            });
        });

        it("should work if object moves into the viewport and out", function (callback) {
            this.slow(200);
            var layoutManager = singleItemLayout([-100, -100, -90, -90]);

            var state = 0;
            var onUpdates = function (viewport, updates) {

                var intersects = rectUtils.intersects(layoutManager.rect, viewport._rect);
                if (state === 0 && !intersects) {
                    assert.deepEqual(updates, {added: [], removed: [], visible: []});
                }
                else if (state === 0 && intersects) {
                    assert.deepEqual(updates, {added: ["theOne"], removed: [], visible: []});
                    state = 1;
                }
                else if (state === 1 && intersects) {
                    assert.deepEqual(updates, {added: [], removed: [], visible: ["theOne"]});
                }
                else if (state === 1 && !intersects) {
                    assert.deepEqual(updates, {added: [], removed: ["theOne"], visible: []});
                    state = 2;
                }
                else if (state == 2){
                    assert.deepEqual(updates, {added: [], removed: [], visible: []});
                    clearInterval(interval);
                    return callback();
                }
            };

            var viewport = X({
                rect: [0, 0, 100, 100],
                rectUtils: rectUtils,
                layoutManager: layoutManager,
                updatesListener: onUpdates
            });

            var interval = setInterval(function () {
                layoutManager.rect = layoutManager.rect.map(function (x) {
                    return x + 10;
                });

                viewport.itemsUpdated({
                    changed: ["theOne"]
                });
            }, 4);
        });
    });

});