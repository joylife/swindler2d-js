var makeWellBehavedLayout = function (layout) {
    var newLayout = Object.create(layout);

    newLayout.findIntersections = function (rect) {
        var This = this;
        var ret = layout.findIntersections.apply(This, arguments);
        return ret || [];
    };

    return newLayout;
};

var VP = function (options) {
    var This = this;
    This._layoutManager = makeWellBehavedLayout(options.layoutManager);
    This._updatesListener = options.updatesListener;
    This._shownKeys = {};
    This.move(options.rect);
};

VP.prototype._addShownKey = function (key) {
    var This = this;
    This._shownKeys[key] = true;
};

VP.prototype._removeShownKey = function (key) {
    var This = this;
    delete This._shownKeys[key];
};

VP.prototype._getShownKeys = function () {
    var This = this;
    return Object.keys(This._shownKeys);
};

var _keyDiff = function (oldKeys, newKeys) {

    var removedKeys = oldKeys.filter(function (oldKey) {
        return newKeys.indexOf(oldKey) === -1;
    });

    var addedKeys = newKeys.filter(function (newKey) {
        return oldKeys.indexOf(newKey) === -1;
    });

    var changedKeys = newKeys.filter(function (newKey) {
        return oldKeys.indexOf(newKey) !== -1;
    });

    var ret = {
        removed: removedKeys,
        added: addedKeys,
        visible: changedKeys
    };

    return ret;
};

VP.prototype._apply = function (rect, update) {
    var This = this;
    var listener = This._updatesListener;
    This._rect = rect;

    update.removed.forEach(This._removeShownKey.bind(This));
    update.added.forEach(This._addShownKey.bind(This));
    listener(This, update);
};

VP.prototype._computeFullUpdate = function (rect) {
    var This = this;
    return _keyDiff(This._getShownKeys(), This._layoutManager.findIntersections(rect));
};

VP.prototype.move = function (rect) {
    var This = this;
    This._apply(rect, This._computeFullUpdate(rect));
};

VP.prototype.itemsUpdated = function (updates) {
    var This = this;
    This._apply(This._rect, This._computeFullUpdate(This._rect));
};

module.exports = function (options) {
    return new VP(options);
};

/**
 * For testing
 */
module.exports._keyDiff = _keyDiff;

